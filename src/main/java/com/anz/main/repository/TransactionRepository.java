package com.anz.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.anz.main.model.RequestBean;;

@Repository
public interface TransactionRepository extends JpaRepository<RequestBean, Integer> {

}
