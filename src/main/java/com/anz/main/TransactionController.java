package com.anz.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anz.main.model.RequestBean;
import com.anz.main.model.ResponseBean;
import com.anz.main.service.TransactionService;

@RestController
@RequestMapping(value = "/api")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	@RequestMapping(value = "/fundtransfer", produces = { MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE })
	public ResponseBean fundTrasnfer(@RequestBody RequestBean fundTransferRequest) {

		boolean response = transactionService.validateData(fundTransferRequest);

		ResponseBean responseBean = new ResponseBean();
		if (response == true) {
			responseBean.setMessageId(1001);
			responseBean.setTxnId(123);
			responseBean.setStatusCode(000);
			
			return responseBean;

		} else {
			responseBean.setMessageId(1001);
			responseBean.setStatusCode(899);
			responseBean.setFailureResponse("Invalid Account Number");
			
			return responseBean;
		}
	}
}
