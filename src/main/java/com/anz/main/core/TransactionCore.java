package com.anz.main.core;

import java.util.Arrays;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.anz.main.model.RequestBean;

@Component
public class TransactionCore {

	public boolean invokeCore(RequestBean transactionBean) {
		
		final String uri = "http://192.168.43.48:8080/core/customer/"+transactionBean.getFromAccount();
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET,null,String.class);		
		System.out.println(result);
		
		return false;
	}
}
