package com.anz.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.anz.main")
@SpringBootApplication
public class AnzFundsTransferAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnzFundsTransferAppApplication.class, args);
	}
}
