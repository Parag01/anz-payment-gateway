package com.anz.main.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anz.main.core.TransactionCore;
import com.anz.main.model.RequestBean;
import com.anz.main.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	TransactionCore transactionCore;

	@SuppressWarnings("unlikely-arg-type")
	public boolean validateData(RequestBean transactionBean) {

		if (transactionRepository.equals(transactionBean))
			return false;
		else {

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			transactionBean.setCurrency("AUD");
			transactionBean.setDateCreated(timestamp);
			transactionBean.setDateModified(timestamp);

			transactionRepository.save(transactionBean);
			// transactionCore.invokeCore(transactionBean);
			return true;
		}
	}

}
