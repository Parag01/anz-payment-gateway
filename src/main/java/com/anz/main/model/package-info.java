/**
 * package-info class handles the XML annotations
 * 
 * @author Parag Gaikwad
 * 
 */

@XmlSchema( 
    namespace = "http://www.hackathon.hcl.com/Transfer", 
    elementFormDefault = XmlNsForm.QUALIFIED)

package com.anz.main.model;
 
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
