package com.anz.main.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FundTransferResponse")
public class ResponseBean {

	private int messageId;
	private int statusCode;
	private int txnId;
	private String failureResponse;

	public ResponseBean() {

	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getTxnId() {
		return txnId;
	}

	public void setTxnId(int txnId) {
		this.txnId = txnId;
	}

	public String getFailureResponse() {
		return failureResponse;
	}

	public void setFailureResponse(String failureResponse) {
		this.failureResponse = failureResponse;
	}

	@Override
	public String toString() {
		return "ResponseBean [messageId=" + messageId + ", statusCode=" + statusCode + ", txnId=" + txnId
				+ ", failureResponse=" + failureResponse + "]";
	}

}
