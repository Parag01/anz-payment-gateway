package com.anz.main.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "FundTransferRequest")
public class RequestBean {

	@Id
	@Column(name = "messageId")
	private int messageId;

	@Column(name = "fromAccount")
	private Integer fromAccount;

	@Column(name = "toAccount")
	private Integer toAccount;

	private String toAccountName;

	@Column(name = "toBsb")
	private Integer toBsb;

	@Column(name = "txnAmount")
	private Integer txnAmount;

	@Column(name = "currency")
	private String currency;

	@Column(name = "dateCreated")
	private Timestamp dateCreated;

	@Column(name = "dateModified")
	private Timestamp dateModified;

	@Column(name = "deleted")
	private boolean deleted;

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public RequestBean() {

	}

	public Integer getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(Integer fromAccount) {
		this.fromAccount = fromAccount;
	}

	public Integer getToAccount() {
		return toAccount;
	}

	public void setToAccount(Integer toAccount) {
		this.toAccount = toAccount;
	}

	public String getToAccountName() {
		return toAccountName;
	}

	public void setToAccountName(String toAccountName) {
		this.toAccountName = toAccountName;
	}

	public Integer getToBsb() {
		return toBsb;
	}

	public void setToBsb(Integer toBsb) {
		this.toBsb = toBsb;
	}

	public Integer getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(Integer txnAmount) {
		this.txnAmount = txnAmount;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "RequestBean [messageId=" + messageId + ", fromAccount=" + fromAccount + ", toAccount=" + toAccount
				+ ", toAccountName=" + toAccountName + ", toBsb=" + toBsb + ", txnAmount=" + txnAmount + ", currency="
				+ currency + ", dateCreated=" + dateCreated + ", dateModified=" + dateModified + ", deleted=" + deleted
				+ "]";
	}

}
